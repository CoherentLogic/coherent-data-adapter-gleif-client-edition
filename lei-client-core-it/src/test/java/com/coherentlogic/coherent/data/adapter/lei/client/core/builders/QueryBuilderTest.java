package com.coherentlogic.coherent.data.adapter.lei.client.core.builders;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEI;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIEntry;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse;

public class QueryBuilderTest {

    private static final Logger log = LoggerFactory.getLogger(QueryBuilderTest.class);

    private final ApplicationContext context
        = new FileSystemXmlApplicationContext ("src/test/resources/spring/application-context.xml");

    private RestTemplate restTemplate = null;

    private QueryBuilder queryBuilder = null;

    @BeforeEach
    public void setUp () throws Exception {

        restTemplate = (RestTemplate) context.getBean (RestTemplate.class);

        queryBuilder = new QueryBuilder (restTemplate);

        //beanDeserializer = context.getBean(FREDBeanDeserializer.class);
    }

    @AfterEach
    public void tearDown () throws Exception {
        restTemplate = null;
        queryBuilder = null;
    }

    @Test
    public void buildURL () {

        String expected = "https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786%2C261700K5E45DJCF5Z735";

        String actual = queryBuilder
            .leirecords()
            .withLEI("35380063V116GJUGS786,261700K5E45DJCF5Z735")
            .getEscapedURI();

        assertEquals (expected, actual);
    }

    @Test
    public void getLEIWithValidURIAndOnlyOneLEI () {

        LEIResponse expectedResponse = new LEIResponse ();

        LEI expectedLEI = new LEI ();

        expectedLEI.setValue("35380063V116GJUGS786");

        LEIEntry leiEntry = new LEIEntry ();

        leiEntry.setLei(expectedLEI);

        expectedResponse.setLeiEntryList( (List<LEIEntry>) Arrays.asList(leiEntry));

        LEIResponse actualResponse =
            queryBuilder.leirecords().withLEI("35380063V116GJUGS786").doGetAsLEIResponse ();

        System.out.println("actualResponse: " + actualResponse);

        assertNotNull (actualResponse);
        //assertEquals (expectedResponse, actualResponse);
    }

    @Test
    public void getLEIWithValidURIAndMultipleLEIs () {

        LEIResponse actualResponse =
            queryBuilder
                .leirecords()
                .withLEI(
                    "261700K5E45DJCF5Z735",
                    "4469000001AVO26P9X86",
                    "5493001KJTIIGC8Y1R12",
                    "39120001KULK7200U106",
                    "EVK05KS7XY1DEII3R011",
                    "48510000JZ17NWGUA510",
                    "029200067A7K6CH0H586",
                    "097900BEFH0000000217",
                    "315700LK78Z7C0WMIL03",
                    "300300KDIZ11PV2GH547",
                    "743700OO8O2N3TQKJC81",
                    "967600100PAMYB9QBQ02",
                    "5299000J2N45DDNE4Y28",
                    "969500Q2MA9VBQ8BG884",
                    "635400DZBUIMTBCXGA12",
                    "353800279ADEFGKNTV65",
                    "9884008RRMX1X5HV6625",
                    "259400L3KBYEVNHEJF55",
                    "335800FVH4MOKZS9VH40",
                    "213800D1EI4B9WTWWD28",
                    "222100T6ICDIY8V4VX70",
                    "253400M18U5TB02TW421",
                    "724500A93Z8V1MJK5349",
                    "959800R2X69K6Y6MX775",
                    "579100KKDDKIFCBKB062",
                    "558600FNC30A8J9EGQ54",
                    "378900F4A0A690EA6735",
                    "789000TVSB96MCOKSB52",
                    "815600EAD78C57FCE690",
                    "7478000050A040C0D041"
                ).doGetAsLEIResponse ();

        System.out.println("actualResponse: " + actualResponse);

        assertNotNull (actualResponse);

//        assertEquals (expectedResponse, actualResponse);
    }
}
/*
261700K5E45DJCF5Z735,4469000001AVO26P9X86,5493001KJTIIGC8Y1R12,39120001KULK7200U106,EVK05KS7XY1DEII3R011,48510000JZ17NWGUA510,029200067A7K6CH0H586,097900BEFH0000000217,315700LK78Z7C0WMIL03,300300KDIZ11PV2GH547,743700OO8O2N3TQKJC81,967600100PAMYB9QBQ02,5299000J2N45DDNE4Y28,969500Q2MA9VBQ8BG884,635400DZBUIMTBCXGA12,353800279ADEFGKNTV65,9884008RRMX1X5HV6625,259400L3KBYEVNHEJF55,335800FVH4MOKZS9VH40,213800D1EI4B9WTWWD28,222100T6ICDIY8V4VX70,253400M18U5TB02TW421,724500A93Z8V1MJK5349,959800R2X69K6Y6MX775,579100KKDDKIFCBKB062,558600FNC30A8J9EGQ54,378900F4A0A690EA6735,789000TVSB96MCOKSB52,815600EAD78C57FCE690,7478000050A040C0D041
*/