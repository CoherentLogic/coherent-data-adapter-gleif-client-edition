package com.coherentlogic.coherent.data.adapter.lei.client.core.configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder;
import com.coherentlogic.coherent.data.adapter.lei.client.core.converters.LEIResponseDeserializer;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse;
import com.coherentlogic.coherent.data.adapter.lei.client.core.factories.GsonFactory;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializer;

@Configuration
public class GlobalConfiguration {

    public static final String
        LEI_MESSAGE_CONVERTER = "leiMessageConverter",
        LEI_RESPONSE_DESERIALIZER = "leiResponseDeserializer",
        LEI_RESPONSE = "leiResponse",
        LEI_GSON_FACTORY = "leiGsonFactory",
        LEI_GSON = "leiGson",
        LEI_REST_TEMPLATE = "leiRestTemplate",
        LEI_QUERY_BUILDER = "leiQueryBuilder";

    @Bean(name=LEI_MESSAGE_CONVERTER)
    public GsonHttpMessageConverter getLEIMessageConverter (@Qualifier(LEI_GSON) Gson leiGson) {

        GsonHttpMessageConverter leiMessageConverter = new GsonHttpMessageConverter ();

        leiMessageConverter.setSupportedMediaTypes (
            Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON_UTF8));

        leiMessageConverter.setGson(leiGson);

        return leiMessageConverter;
    }

    @Bean(name=LEI_RESPONSE_DESERIALIZER)
    public LEIResponseDeserializer getLEIResponseDeserializer () {
        return new LEIResponseDeserializer ();
    }

    @Bean(name=LEI_GSON_FACTORY)
    public GsonFactory getLEIGsonFactory (
        @Qualifier(LEI_RESPONSE_DESERIALIZER) LEIResponseDeserializer leiResponseDeserializer) {

        Map<Class<?>, JsonDeserializer<?>> typeAdapterMap = new HashMap<Class<?>, JsonDeserializer<?>> ();

        typeAdapterMap.put(LEIResponse.class, leiResponseDeserializer);

        GsonFactory leiGsonFactory = new GsonFactory (typeAdapterMap);

        return leiGsonFactory;
    }

    @Bean(name=LEI_GSON)
    public Gson getLEIGson (@Qualifier(LEI_GSON_FACTORY) GsonFactory leiGsonFactory) {
        return leiGsonFactory.getInstance();
    }

    @Bean(name=LEI_REST_TEMPLATE)
    public RestTemplate getLEIRestTemplate (
        @Qualifier(LEI_MESSAGE_CONVERTER) GsonHttpMessageConverter leiMessageConverter) {

        RestTemplate leiRestTemplate = new RestTemplate ();

        leiRestTemplate.setMessageConverters(Arrays.asList(leiMessageConverter));

        return leiRestTemplate;
    }

    @Bean(name=LEI_RESPONSE)
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public LEIResponse getLEIResponse () {
        return new LEIResponse ();
    }

    @Bean(name=LEI_QUERY_BUILDER)
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public QueryBuilder getLEIQueryBuilder (
        @Qualifier(LEI_REST_TEMPLATE) RestTemplate leiRestTemplate) {
        return new QueryBuilder (leiRestTemplate);
    }
}
