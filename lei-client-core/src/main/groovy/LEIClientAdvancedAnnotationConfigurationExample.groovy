/* See https://issues.jboss.org/browse/RESTEASY-1601
 */

@GrabResolver(name='JBoss Release Repository', root='https://repository.jboss.org/nexus/content/repositories/releases/')
@GrabResolver(name='JBoss.org Maven repository', root='https://repository.jboss.org/nexus/content/groups/public')

@GrabExclude(group = 'org.jboss.spec.javax.ws.rs', module='jboss-jaxrs-api_2.1_spec')
@GrabExclude(group = 'org.jboss.spec.javax.xml.bind', module='jboss-jaxb-api_2.3_spec')
@GrabExclude(group = 'org.jboss.spec.javax.servlet', module='jboss-servlet-api_3.1_spec')
@GrabExclude(group = 'javax.validation', module='validation-api')
@GrabExclude(group = 'org.jboss.spec.javax.annotation', module='jboss-annotations-api_1.2_spec')
@GrabExclude(group = 'net.jcip', module='jcip-annotations')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-processor')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-annotations')
@GrabExclude(group = 'javax.activation', module='activation')
@GrabExclude(group = 'junit', module='junit')
//@GrabExclude(group='org.infinispan', module='infinispan-core')
//@GrabExclude(group='org.infinispan', module='infinispan-clustered-lock')

@Grab(group='org.jboss.logging', module='jboss-logging', version='3.3.0.Final')
@Grab(group='org.apache.logging.log4j', module='log4j-slf4j-impl', version='2.11.1')
//@Grab(group='org.slf4j', module='slf4j-api', version='1.7.7.jbossorg-1')

@GrabExclude(group = 'org.infinispan', module='infinispan-core')
@GrabExclude(group = 'org.infinispan', module='infinispan-clustered-lock')
@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='com.coherentlogic.enterprise-data-adapter.infinispan-int', module='infinispan-int-core-default-local-cache', version='3.0.0-RELEASE')
//@Grab(group='org.infinispan', module='infinispan-embedded', version='9.1.7.Final')
//@Grab(group='org.infinispan', module='infinispan-core', version='9.4.6.Final')
//@Grab(group='org.infinispan', module='infinispan-commons', version='9.4.6.Final')

// Should not be required when running a local cache and/or should be a transitive dependency.
//@Grab(group='org.jgroups', module='jgroups', version='4.0.15.Final')
@Grab(group='org.reactivestreams', module='reactive-streams', version='1.0.2')
@Grab(group='javax.transaction', module='jta', version='1.1')
import org.infinispan.manager.DefaultCacheManager
import org.infinispan.manager.EmbeddedCacheManager
//import org.infinispan.commons.tx.lookup.TransactionManagerLookup

import java.io.InputStream
import java.io.ByteArrayInputStream

@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='com.jamonapi', module='jamon', version='2.81')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-core', version='3.0.0-RELEASE')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-configuration', version='3.0.0-RELEASE')
import com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEventListener

@Grab(group='org.springframework', module='spring-beans', version='5.1.4.RELEASE')
@Grab(group='org.springframework', module='spring-context', version='5.1.4.RELEASE')
@Grab(group='org.springframework', module='spring-aop', version='5.1.4.RELEASE')
import org.springframework.core.io.ByteArrayResource
import org.springframework.context.support.GenericXmlApplicationContext

@Grab(group='com.jamonapi', module='jamon', version='2.81')
import com.jamonapi.Monitor
import com.jamonapi.MonitorFactory

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean
import com.coherentlogic.infinispan.cache.providers.DefaultInfinispanCacheServiceProviderFactory

import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent

import groovy.swing.SwingBuilder 
import javax.swing.*
import java.awt.*

def applicationContextDefinition = '''
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xmlns:util="http://www.springframework.org/schema/util"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/util
        http://www.springframework.org/schema/util/spring-util-3.0.xsd">

    <context:component-scan base-package="com.coherentlogic.infinispan.cache.configurations.local, com.coherentlogic.coherent.data.adapter.lei.client" />

    <bean id="leiResponse" class="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse" scope="prototype"/>
    <bean id="leiEntry" class="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIEntry" scope="prototype"/>
    <bean id="lei" class="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEI" scope="prototype"/>

    <bean id="leiResponseDeserializer" class="com.coherentlogic.coherent.data.adapter.lei.client.core.converters.LEIResponseDeserializer"/>

    <bean id="leiGsonFactory" class="com.coherentlogic.coherent.data.adapter.lei.client.core.factories.GsonFactory">
        <constructor-arg name="typeAdapterMap">
            <util:map>
                <entry key="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse" value-ref="leiResponseDeserializer"/>
            </util:map>
        </constructor-arg>
    </bean>

    <bean id="leiGson" factory-bean="leiGsonFactory" factory-method="getInstance" />

    <bean id="leiRestTemplate" class="org.springframework.web.client.RestTemplate">
        <property name="messageConverters">
            <list>
                <bean id="leiGsonHttpMessageConverter"
                 class="org.springframework.http.converter.json.GsonHttpMessageConverter">
                    <property name="supportedMediaTypes">
                        <util:list>
                            <value>#{T(org.springframework.http.MediaType).APPLICATION_JSON}</value>
                            <value>#{T(org.springframework.http.MediaType).APPLICATION_JSON_UTF8}</value>
                        </util:list>
                    </property>
                    <property name="gson" ref="leiGson"/>
                </bean>
            </list>
        </property>
    </bean>

    <bean id="queryBuilder" class="com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder" scope="prototype">
        <constructor-arg name="restTemplate" ref="leiRestTemplate"/>
        <constructor-arg name="cache" ref="#{T(com.coherentlogic.infinispan.cache.configurations.local.InfinispanDefaultLocalConfiguration).DEFAULT_LOCAL_INFINISPAN_CACHE_SERVICE_PROVIDER_BEAN_NAME}"/>
    </bean>
</beans>
'''

def resource = new ByteArrayResource(applicationContextDefinition.getBytes())

def applicationContext = new GenericXmlApplicationContext(resource)

def eventListener = new QueryBuilderEventListener<String> () {

    def ctr = 0

    def totalMillisOverall = 0

    @Override
    public void onEvent(QueryBuilderEvent<String, ? extends SerializableBean> event) {

        if (QueryBuilderEvent.EventType.methodEnds == event.getEventType ()) {

            def totalMillisThisStep = event.getOperationAtThisStepMillis() - event.getOperationBeganAtMillis()

            totalMillisOverall += totalMillisThisStep

            println "----- event[ @ ${ctr++}]: ${event.getEventType ()}, totalMillisThisStep (ms): $totalMillisThisStep, totalMillisOverall: $totalMillisOverall"
        } else
            println "----- event[ @ ${ctr}]: ${event.getEventType ()}"
    }
}

def printResponse (leiResponse) {
    println ""
    println "----- ----- ----- ----- ----- ----- ----- ----- ----- -----"
    println "leiResponse: ${leiResponse}"
}

def queryBuilder = applicationContext.getBean("queryBuilder")

queryBuilder.addQueryBuilderEventListener (eventListener)

// https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786
def leiResponse1 = queryBuilder.leirecords().withLEI("35380063V116GJUGS786").doGetAsLEIResponse ()

@groovy.beans.Bindable
class Address { 
    String street, number, city
    String toString() { "address[street=$street,number=$number,city=$city]" }
}

def address = new Address(street: 'Evergreen Terrace', number: '742', city: 'Springfield')

// Create a builder 
def ui = new SwingBuilder()

def frame = ui.frame(title : 'Coherent Data Adapter: GLEIF Client Middleware for Java Demonstration Application', location : [200, 200], 
    size : [400, 300], defaultCloseOperation : WindowConstants.DISPOSE_ON_CLOSE) {
       panel(constraints: BorderLayout.CENTER,
                border: compoundBorder([emptyBorder(10), titledBorder('Enter your address:')])) {
            tableLayout {
                tr {
                    td {
                        label 'Street:'  // text property is default, so it is implicit.
                    }
                    td {
                        textField address.street, id: 'streetField', columns: 20
                    }
                }
                tr {
                    td {
                        label 'Number:'
                    }
                    td {
                        textField id: 'numberField', columns: 5, text: address.number
                    }
                }
                tr {
                    td {
                        label 'City:'
                    }
                    td {
                        textField id: 'cityField', columns: 20, address.city
                    }
                }
            }
        }
    }
    
// The following  statement is used for displaying the form 
frame.setVisible(true)

printResponse (leiResponse1)

queryBuilder = applicationContext.getBean("queryBuilder")

queryBuilder.addQueryBuilderEventListener (eventListener)

def leiResponse2 = queryBuilder.replacePath("/leirecords").replaceQuery ("lei=35380063V116GJUGS786").doGetAsLEIResponse ()

printResponse (leiResponse2)

queryBuilder = applicationContext.getBean("queryBuilder")

queryBuilder.addQueryBuilderEventListener (eventListener)

// NOTE: "lei=${0}" is a GString and will be interpolated as "lei=0" whereas 'lei=${0}' is a string and will *not be interpolated*.

def leiResponse3 = queryBuilder.replacePath("/leirecords").replaceQuery ('lei=${0}', "35380063V116GJUGS786").doGetAsLEIResponse ()

printResponse (leiResponse3)

for (ctr in 0..10) {

    queryBuilder = applicationContext.getBean("queryBuilder")

    queryBuilder.addQueryBuilderEventListener (eventListener)

    def leiResponseX = queryBuilder.replacePath("/leirecords").replaceQuery ('lei=${0}', "35380063V116GJUGS786").doGetAsLEIResponse ()
}

applicationContext.close()