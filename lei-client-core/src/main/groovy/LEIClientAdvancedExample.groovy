/* See https://issues.jboss.org/browse/RESTEASY-1601
 */

@GrabResolver(name='JBoss Release Repository', root='https://repository.jboss.org/nexus/content/repositories/releases/')
@GrabResolver(name='JBoss.org Maven repository', root='https://repository.jboss.org/nexus/content/groups/public')

@GrabExclude(group = 'org.jboss.spec.javax.ws.rs', module='jboss-jaxrs-api_2.1_spec')
@GrabExclude(group = 'org.jboss.spec.javax.xml.bind', module='jboss-jaxb-api_2.3_spec')
@GrabExclude(group = 'org.jboss.spec.javax.servlet', module='jboss-servlet-api_3.1_spec')
@GrabExclude(group = 'javax.validation', module='validation-api')
@GrabExclude(group = 'org.jboss.spec.javax.annotation', module='jboss-annotations-api_1.2_spec')
@GrabExclude(group = 'net.jcip', module='jcip-annotations')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-processor')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-annotations')
@GrabExclude(group = 'javax.activation', module='activation')
@GrabExclude(group = 'junit', module='junit')

@Grab(group='org.jboss.logging', module='jboss-logging', version='3.3.0.Final')
@Grab(group='org.slf4j', module='slf4j-api', version='1.7.7.jbossorg-1')
// grape install com.github.ben-manes.caffeine caffeine 2.4.0
//@Grab(group='com.github.ben-manes.caffeine', module='caffeine', version='2.4.0')

@GrabExclude(group = 'org.infinispan', module='infinispan-core')
@GrabExclude(group = 'org.infinispan', module='infinispan-clustered-lock')
@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='com.coherentlogic.enterprise-data-adapter', module='infinispan-int', version='3.0.0-RELEASE')
@Grab(group='org.infinispan', module='infinispan-embedded', version='9.1.7.Final')
@Grab(group='org.reactivestreams', module='reactive-streams', version='1.0.2')
@Grab(group='javax.transaction', module='jta', version='1.1')
import org.infinispan.manager.DefaultCacheManager
import org.infinispan.manager.EmbeddedCacheManager

import java.io.InputStream
import java.io.ByteArrayInputStream

@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='com.jamonapi', module='jamon', version='2.81')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-core', version='3.0.0-RELEASE')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-config', version='3.0.0-RELEASE')
import com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEventListener

@Grab(group='org.springframework', module='spring-beans', version='5.1.4.RELEASE')
@Grab(group='org.springframework', module='spring-context', version='5.1.4.RELEASE')
@Grab(group='org.springframework', module='spring-aop', version='5.1.4.RELEASE')
import org.springframework.core.io.ByteArrayResource
import org.springframework.context.support.GenericXmlApplicationContext

@Grab(group='com.jamonapi', module='jamon', version='2.81')
import com.jamonapi.Monitor
import com.jamonapi.MonitorFactory

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean
import com.coherentlogic.infinispan.cache.providers.DefaultInfinispanCacheServiceProviderFactory

import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent

public class CacheManagerFactory {

    public EmbeddedCacheManager getInstance () {
        // <!-- OPTIMISTIC: org.infinispan.InvalidCacheUsageException: Explicit locking is not allowed with optimistic caches! -->
        return new DefaultCacheManager (
            new ByteArrayInputStream (
                '''<infinispan
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:schemaLocation="urn:infinispan:config:9.1 http://infinispan.org/schemas/infinispan-config-9.1.xsd"
                    xmlns="urn:infinispan:config:9.1">
                       <cache-container default-cache="localLEICache">
                           <local-cache name="localLEICache">
                               <transaction
                                transaction-manager-lookup="org.infinispan.transaction.lookup.GenericTransactionManagerLookup"
                                stop-timeout="30000"
                                auto-commit="true"
                                locking="PESSIMISTIC"
                                mode="FULL_XA"/>
                           </local-cache>
                       </cache-container>
                   </infinispan>'''.getBytes ()
            )
        )
    }
}

public class InfinispanCacheServiceProviderFactory extends DefaultInfinispanCacheServiceProviderFactory {

    public InfinispanCacheServiceProviderFactory(EmbeddedCacheManager cacheManager) {
        super(cacheManager, "localLEICache")
    }
}

def applicationContextDefinition = '''
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xmlns:util="http://www.springframework.org/schema/util"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/util
        http://www.springframework.org/schema/util/spring-util-3.0.xsd">

    <context:component-scan base-package="com.coherentlogic.coherent.data.adapter.lei.client" />

    <bean id="leiResponse" class="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse" scope="prototype"/>
    <bean id="leiEntry" class="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIEntry" scope="prototype"/>
    <bean id="lei" class="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEI" scope="prototype"/>

    <bean id="leiResponseDeserializer" class="com.coherentlogic.coherent.data.adapter.lei.client.core.converters.LEIResponseDeserializer"/>

    <bean id="leiGsonFactory" class="com.coherentlogic.coherent.data.adapter.lei.client.core.factories.GsonFactory">
        <constructor-arg name="typeAdapterMap">
            <util:map>
                <entry key="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse" value-ref="leiResponseDeserializer"/>
            </util:map>
        </constructor-arg>
    </bean>

    <bean id="leiGson" factory-bean="leiGsonFactory" factory-method="getInstance" />

    <bean id="leyCacheManagerFactory" class="CacheManagerFactory"/>

    <bean id="leiCacheManager" class="org.infinispan.manager.DefaultCacheManager" factory-bean="leyCacheManagerFactory" factory-method="getInstance" init-method="start" destroy-method="stop"/>

    <bean id="infinispanCacheServiceProviderFactory" class="InfinispanCacheServiceProviderFactory">
         <constructor-arg name="leiCacheManager" ref="leiCacheManager"/>
    </bean>

    <bean id="leiCacheServiceProvider"
     class="com.coherentlogic.infinispan.cache.providers.DefaultInfinispanCacheServiceProvider"
     factory-bean="infinispanCacheServiceProviderFactory"
     factory-method="getInstance"/>

    <bean id="leiRestTemplate" class="org.springframework.web.client.RestTemplate">
        <property name="messageConverters">
            <list>
                <bean id="leiGsonHttpMessageConverter"
                 class="org.springframework.http.converter.json.GsonHttpMessageConverter">
                    <property name="supportedMediaTypes">
                        <util:list>
                            <value>#{T(org.springframework.http.MediaType).APPLICATION_JSON}</value>
                            <value>#{T(org.springframework.http.MediaType).APPLICATION_JSON_UTF8}</value>
                        </util:list>
                    </property>
                    <property name="gson" ref="leiGson"/>
                </bean>
            </list>
        </property>
    </bean>

    <bean id="queryBuilder" class="com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder" scope="prototype">
        <constructor-arg name="restTemplate" ref="leiRestTemplate"/>
        <constructor-arg name="cache" ref="leiCacheServiceProvider"/>
    </bean>
</beans>
'''

def resource = new ByteArrayResource(applicationContextDefinition.getBytes())

def applicationContext = new GenericXmlApplicationContext(resource)

def eventListener = new QueryBuilderEventListener<String> () {

    def totalMillisOverall = 0

    @Override
    public void onEvent(QueryBuilderEvent<String, ? extends SerializableBean> event) {

        if (QueryBuilderEvent.EventType.methodEnds == event.getEventType ()) {

            def totalMillisThisStep = event.getOperationAtThisStepMillis() - event.getOperationBeganAtMillis()

            totalMillisOverall += totalMillisThisStep

            println "----- event: ${event.getEventType ()}, totalMillisThisStep (ms): $totalMillisThisStep, totalMillisOverall: $totalMillisOverall"
        } else
            println "----- event: ${event.getEventType ()}"
    }
}

def printResponse (leiResponse) {
    println ""
    println "----- ----- ----- ----- ----- ----- ----- ----- ----- -----"
    println "leiResponse: ${leiResponse}"
}

def queryBuilder = applicationContext.getBean(QueryBuilder.class)

queryBuilder.addQueryBuilderEventListener (eventListener)

// https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786
def leiResponse1 = queryBuilder.leirecords().withLEI("35380063V116GJUGS786").doGetAsLEIResponse ()

printResponse (leiResponse1)

queryBuilder = applicationContext.getBean(QueryBuilder.class)

queryBuilder.addQueryBuilderEventListener (eventListener)

def leiResponse2 = queryBuilder.replacePath("/leirecords").replaceQuery ("lei=35380063V116GJUGS786").doGetAsLEIResponse ()

printResponse (leiResponse2)

queryBuilder = applicationContext.getBean(QueryBuilder.class)

queryBuilder.addQueryBuilderEventListener (eventListener)

// NOTE: "lei=${0}" is a GString and will be interpolated as "lei=0" whereas 'lei=${0}' is a string and will *not be interpolated*.

def leiResponse3 = queryBuilder.replacePath("/leirecords").replaceQuery ('lei=${0}', "35380063V116GJUGS786").doGetAsLEIResponse ()

printResponse (leiResponse3)

for (ctr in 0..10) {

    queryBuilder = applicationContext.getBean(QueryBuilder.class)

    queryBuilder.addQueryBuilderEventListener (eventListener)

    def leiResponseX = queryBuilder.replacePath("/leirecords").replaceQuery ('lei=${0}', "35380063V116GJUGS786").doGetAsLEIResponse ()
}

applicationContext.close()