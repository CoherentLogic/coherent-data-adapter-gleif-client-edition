@GrabExclude(group = 'org.jboss.spec.javax.ws.rs', module='jboss-jaxrs-api_2.1_spec')
@GrabExclude(group = 'org.jboss.spec.javax.xml.bind', module='jboss-jaxb-api_2.3_spec')
@GrabExclude(group = 'org.jboss.spec.javax.servlet', module='jboss-servlet-api_3.1_spec')
@GrabExclude(group = 'javax.validation', module='validation-api')
@GrabExclude(group = 'org.jboss.spec.javax.annotation', module='jboss-annotations-api_1.2_spec')
@GrabExclude(group = 'net.jcip', module='jcip-annotations')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-processor')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-annotations')
@GrabExclude(group = 'org.reactivestreams', module='reactive-streams')
@GrabExclude(group = 'javax.activation', module='activation')
@GrabExclude(group = 'junit', module='junit')

@GrabResolver(name='JBoss Release Repository', root='https://repository.jboss.org/nexus/content/repositories/releases/')
@GrabResolver(name='JBoss.org Maven repository', root='https://repository.jboss.org/nexus/content/groups/public')

@Grab(group='org.jboss.logging', module='jboss-logging-processor', version='3.3.2.Final')
@Grab(group='org.jboss.logging', module='jboss-logging-annotations', version='3.3.2.Final')
//@Grab(group='org.jboss.spec.javax.servlet', module='jboss-servlet-api_3.1_spec', version='3.3.2.Final')
@Grab(group='org.jboss.spec.javax.ws.rs', module='jboss-jaxrs-api_2.1_spec', version='1.0.0.Final')
@Grab(group='javax.activation', module='activation', version='1.1.1')
@Grab(group='javax.validation', module='validation-api', version='1.1.0.Final')

@Grab(group='org.infinispan', module='infinispan-core', version='9.4.5.Final')
@Grab(group='org.infinispan', module='infinispan-commons', version='9.4.5.Final')
@Grab(group='org.infinispan', module='infinispan-query', version='9.4.5.Final')
@Grab(group='com.coherentlogic.enterprise-data-adapter', module='infinispan-int', version='3.0.0-RELEASE')
import org.infinispan.manager.EmbeddedCacheManager

@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='com.jamonapi', module='jamon', version='2.81')
@Grab(group='com.coherentlogic.enterprise-data-adapter', module='aspectj-int', version='3.0.0-RELEASE')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-core', version='3.0.0-RELEASE')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-config', version='3.0.0-RELEASE')
import com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder

@Grab(group='org.springframework', module='spring-context', version='5.1.4.RELEASE')
import org.springframework.core.io.ByteArrayResource
import org.springframework.context.support.GenericXmlApplicationContext

import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.notifications.Listener;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryVisited;
import org.infinispan.notifications.cachelistener.event.CacheEntryVisitedEvent;
import org.infinispan.notifications.cachemanagerlistener.annotation.ViewChanged;
import org.infinispan.notifications.cachemanagerlistener.event.ViewChangedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.infinispan.cache.providers.DefaultInfinispanCacheServiceProviderFactory;

/**
 * Defines an Infinispan cache manager with the cache mode set to DIST_SYNC (distributed sync) and 
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class InfinispanCacheServiceProviderFactory extends DefaultInfinispanCacheServiceProviderFactory {

    public static final String DEFAULT_CACHE_NAME = "leiCache";

    public InfinispanCacheServiceProviderFactory(EmbeddedCacheManager cacheManager) {
        this (cacheManager, DEFAULT_CACHE_NAME);
    }

    public InfinispanCacheServiceProviderFactory(
        EmbeddedCacheManager cacheManager,
        String defaultCacheName
    ) {
        super(cacheManager, defaultCacheName);
    }
}

def applicationContextDefinition = '''
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xmlns:util="http://www.springframework.org/schema/util"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/util
        http://www.springframework.org/schema/util/spring-util-3.0.xsd">

    <context:component-scan base-package="com.coherentlogic.coherent.data.adapter.lei.client" />

    <bean id="leiResponse" class="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse" scope="prototype"/>
    <bean id="leiEntry" class="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIEntry" scope="prototype"/>
    <bean id="lei" class="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEI" scope="prototype"/>

    <bean id="leiResponseDeserializer" class="com.coherentlogic.coherent.data.adapter.lei.client.core.converters.LEIResponseDeserializer"/>

    <bean id="leiGsonFactory" class="com.coherentlogic.coherent.data.adapter.lei.client.core.factories.GsonFactory">
        <constructor-arg name="typeAdapterMap">
            <util:map>
                <entry key="com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse" value-ref="leiResponseDeserializer"/>
            </util:map>
        </constructor-arg>
    </bean>

    <bean id="leiGson" factory-bean="leiGsonFactory" factory-method="getInstance" />

    <bean id="infinispanCacheServiceProviderFactory"
     class="InfinispanCacheServiceProviderFactory">
         <!-- These two args are instantiated in the JavaConfiguration class. -->
         <constructor-arg value="leiCacheManager"/>
         <constructor-arg  name="defaultCacheName" value="leiCacheManager"/>
    </bean>

    <bean id="leiCacheServiceProvider"
     class="com.coherentlogic.infinispan.cache.providers.DefaultInfinispanCacheServiceProvider"
     factory-bean="infinispanCacheServiceProviderFactory"
     factory-method="getInstance"/>

    <bean id="leiRestTemplate" class="org.springframework.web.client.RestTemplate">
        <property name="messageConverters">
            <list>
                <bean id="leiGsonHttpMessageConverter"
                 class="org.springframework.http.converter.json.GsonHttpMessageConverter">
                    <property name="supportedMediaTypes">
                        <util:list>
                            <value>#{T(org.springframework.http.MediaType).APPLICATION_JSON}</value>
                            <value>#{T(org.springframework.http.MediaType).APPLICATION_JSON_UTF8}</value>
                        </util:list>
                    </property>
                    <property name="gson" ref="leiGson"/>
                </bean>
            </list>
        </property>
    </bean>

    <bean id="queryBuilder" class="com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder" scope="prototype">
        <constructor-arg name="restTemplate" ref="leiRestTemplate"/>
        <constructor-arg name="cache" ref="cache"/>
    </bean>
</beans>
'''

def resource = new ByteArrayResource(applicationContextDefinition.getBytes())

def applicationContext = new GenericXmlApplicationContext(resource)

def queryBuilder = applicationContext.getBean(QueryBuilder.class)

// https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786
def leiResponse1 = queryBuilder.leirecords().withLEI("35380063V116GJUGS786").doGetAsLEIResponse ()

println ""
println "----- ----- ----- ----- ----- ----- ----- ----- ----- -----"
println "leiResponse1: ${leiResponse1}"

queryBuilder = applicationContext.getBean(QueryBuilder.class)

def leiResponse2 = queryBuilder.replacePath("/leirecords").replaceQuery ("lei=35380063V116GJUGS786").doGetAsLEIResponse ()

println ""
println "----- ----- ----- ----- ----- ----- ----- ----- ----- -----"
println "leiResponse2: ${leiResponse2}"

queryBuilder = applicationContext.getBean(QueryBuilder.class)

// NOTE: "lei=${0}" is a GString and will be interpolated as "lei=0" whereas 'lei=${0}' is a string and will *not be interpolated*.

def leiResponse3 = queryBuilder.replacePath("/leirecords").replaceQuery ('lei=${0}', "35380063V116GJUGS786").doGetAsLEIResponse ()

println ""
println "----- ----- ----- ----- ----- ----- ----- ----- ----- -----"
println "leiResponse3: ${leiResponse3}"

applicationContext.close()