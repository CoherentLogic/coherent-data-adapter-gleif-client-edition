/* See https://issues.jboss.org/browse/RESTEASY-1601
 */

@GrabResolver(name='JBoss Release Repository', root='https://repository.jboss.org/nexus/content/repositories/releases/')
@GrabResolver(name='JBoss.org Maven repository', root='https://repository.jboss.org/nexus/content/groups/public')

@GrabExclude(group = 'org.jboss.spec.javax.ws.rs', module='jboss-jaxrs-api_2.1_spec')
@GrabExclude(group = 'org.jboss.spec.javax.xml.bind', module='jboss-jaxb-api_2.3_spec')
@GrabExclude(group = 'org.jboss.spec.javax.servlet', module='jboss-servlet-api_3.1_spec')
@GrabExclude(group = 'javax.validation', module='validation-api')
@GrabExclude(group = 'org.jboss.spec.javax.annotation', module='jboss-annotations-api_1.2_spec')
@GrabExclude(group = 'net.jcip', module='jcip-annotations')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-processor')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-annotations')
@GrabExclude(group = 'javax.activation', module='activation')
@GrabExclude(group = 'junit', module='junit')

@GrabExclude(group = 'org.infinispan', module='infinispan-core')
@GrabExclude(group = 'org.infinispan', module='infinispan-clustered-lock')

@Grab(group='org.jboss.logging', module='jboss-logging', version='3.3.0.Final')
@Grab(group='org.apache.logging.log4j', module='log4j-slf4j-impl', version='2.11.1')
@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='org.reactivestreams', module='reactive-streams', version='1.0.2')
@Grab(group='javax.transaction', module='jta', version='1.1')
@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='org.springframework', module='spring-beans', version='5.1.4.RELEASE')
@Grab(group='org.springframework', module='spring-context', version='5.1.4.RELEASE')
@Grab(group='org.springframework', module='spring-aop', version='5.1.4.RELEASE')
@Grab(group='com.coherentlogic.enterprise-data-adapter.infinispan-int', module='infinispan-int-core-default-local-cache', version='3.0.0-RELEASE')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-core', version='3.0.0-RELEASE')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-configuration', version='3.0.0-RELEASE')

import org.infinispan.manager.DefaultCacheManager
import org.infinispan.manager.EmbeddedCacheManager

import java.io.InputStream
import java.io.ByteArrayInputStream

import com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEventListener

import org.springframework.core.io.ByteArrayResource
import org.springframework.context.support.GenericXmlApplicationContext

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean
import com.coherentlogic.infinispan.cache.providers.DefaultInfinispanCacheServiceProviderFactory

import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent

def applicationContextDefinition = '''
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd">

    <context:component-scan base-package="com.coherentlogic.infinispan.cache.configurations.local, com.coherentlogic.coherent.data.adapter.lei.client" />

    <bean id="localCacheEnabledQueryBuilder" class="com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder" scope="prototype">
        <constructor-arg name="restTemplate" ref="#{T(com.coherentlogic.coherent.data.adapter.lei.client.core.configuration.GlobalConfiguration).LEI_REST_TEMPLATE}"/>
        <constructor-arg name="cache" ref="#{T(com.coherentlogic.infinispan.cache.configurations.local.InfinispanDefaultLocalConfiguration).DEFAULT_LOCAL_INFINISPAN_CACHE_SERVICE_PROVIDER_BEAN_NAME}"/>
    </bean>
</beans>
'''

def applicationContext = new GenericXmlApplicationContext(
    new ByteArrayResource(applicationContextDefinition.getBytes())
)

def queryBuilder = applicationContext.getBean("localCacheEnabledQueryBuilder")

// https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786
def leiResponse = queryBuilder.leirecords().withLEI("35380063V116GJUGS786").doGetAsLEIResponse ()

println "leiResponse: $leiResponse"

applicationContext.close()