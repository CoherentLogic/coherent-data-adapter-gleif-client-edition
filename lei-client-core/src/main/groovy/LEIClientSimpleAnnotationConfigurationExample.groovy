@GrabExclude(group = 'org.jboss.spec.javax.ws.rs', module='jboss-jaxrs-api_2.1_spec')
@GrabExclude(group = 'org.jboss.spec.javax.xml.bind', module='jboss-jaxb-api_2.3_spec')
@GrabExclude(group = 'org.jboss.spec.javax.servlet', module='jboss-servlet-api_3.1_spec')
@GrabExclude(group = 'javax.validation', module='validation-api')
@GrabExclude(group = 'org.jboss.spec.javax.annotation', module='jboss-annotations-api_1.2_spec')
@GrabExclude(group = 'net.jcip', module='jcip-annotations')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-processor')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-annotations')
@GrabExclude(group = 'javax.activation', module='activation')
@GrabExclude(group = 'org.reactivestreams', module='reactive-streams')
@GrabExclude(group = 'org.jgroups', module='jgroups')
@GrabExclude(group = 'junit', module='junit')

@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-configuration', version='3.0.0-RELEASE')
import com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder

@Grab(group='org.springframework', module='spring-context', version='5.1.4.RELEASE')
import org.springframework.context.annotation.AnnotationConfigApplicationContext

def applicationContext = new AnnotationConfigApplicationContext ("com.coherentlogic.coherent.data.adapter.lei.client.core.configuration")

def queryBuilder = applicationContext.getBean(QueryBuilder.class)

// The next line is the equivalent of https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786
def result = queryBuilder.leirecords().withLEI("35380063V116GJUGS786").doGetAsLEIResponse ()

println ("result: $result")

applicationContext.close ()