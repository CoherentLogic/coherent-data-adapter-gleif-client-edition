@GrabExclude(group = 'org.jboss.spec.javax.ws.rs', module='jboss-jaxrs-api_2.1_spec')
@GrabExclude(group = 'org.jboss.spec.javax.xml.bind', module='jboss-jaxb-api_2.3_spec')
@GrabExclude(group = 'org.jboss.spec.javax.servlet', module='jboss-servlet-api_3.1_spec')
@GrabExclude(group = 'javax.validation', module='validation-api')
@GrabExclude(group = 'org.jboss.spec.javax.annotation', module='jboss-annotations-api_1.2_spec')
@GrabExclude(group = 'net.jcip', module='jcip-annotations')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-processor')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-annotations')
@GrabExclude(group = 'javax.activation', module='activation')
@GrabExclude(group = 'org.reactivestreams', module='reactive-streams')
@GrabExclude(group = 'org.jgroups', module='jgroups')
@GrabExclude(group = 'junit', module='junit')

@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='com.coherentlogic.lei.client', module='lei-client-core', version='3.0.0-RELEASE')
import com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder
import com.coherentlogic.coherent.data.adapter.lei.client.core.factories.GsonFactory
import com.coherentlogic.coherent.data.adapter.lei.client.core.converters.LEIResponseDeserializer
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse

@Grab(group='org.springframework.ws', module='spring-ws-core', version='3.0.6.RELEASE')
import org.springframework.web.client.RestTemplate
import org.springframework.http.converter.json.GsonHttpMessageConverter
import org.springframework.http.MediaType

def leiMessageConverter = new GsonHttpMessageConverter ()

leiMessageConverter.setSupportedMediaTypes ([MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON_UTF8])

def leiResponseDeserializer = new LEIResponseDeserializer ()

def typeAdapterMap = [:]

typeAdapterMap.put (LEIResponse.class, leiResponseDeserializer)

def leiGsonFactory = new GsonFactory (typeAdapterMap)

def leiGson = leiGsonFactory.getInstance ()

leiMessageConverter.setGson(leiGson)

def restTemplate = new RestTemplate ()

restTemplate.setMessageConverters ([leiMessageConverter])

def queryBuilder = new QueryBuilder (restTemplate)

// The next line is the equivalent of https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786
queryBuilder.leirecords().withLEI("35380063V116GJUGS786").doGetAsLEIResponse ()

