package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;

/*
"OtherEntityNames": {
        "OtherEntityName": [{
            "@xml:lang": "en",
            "@type": "ALTERNATIVE_LANGUAGE_LEGAL_NAME",
            "$": "TUBOMIKENSHOKU CO.,LTD"
        }]
    },
 */
public class OtherEntityName extends SerializableValueBean<String> implements XMLLangBeanSpecification {

    public static final String OTHER_ENTITY_NAME_KEY = "OtherEntityName", TYPE_KEY = "@type";

    /**
     * 
     */
    private static final long serialVersionUID = -157519911005814641L;

    /**
     * @TODO Convert to an enum?
     */
    private String type;

    /**
     * @TODO Convert to an enum?
     */
    private String xmlLang;

    @Override
    public String getXMLLang() {
        return xmlLang;
    }

    @Override
    public void setXMLLang(@Changeable (XMLLANG) String xmlLang) {
        this.xmlLang = xmlLang;
    }

    public String getType() {
        return type;
    }

    public static final String TYPE = "type";

    public void setType(@Changeable(TYPE) String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((xmlLang == null) ? 0 : xmlLang.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        OtherEntityName other = (OtherEntityName) obj;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (xmlLang == null) {
            if (other.xmlLang != null)
                return false;
        } else if (!xmlLang.equals(other.xmlLang))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "OtherEntityName [type=" + type + ", xmlLang=" + xmlLang + ", toString()="
            + super.toString() + "]";
    }
}
