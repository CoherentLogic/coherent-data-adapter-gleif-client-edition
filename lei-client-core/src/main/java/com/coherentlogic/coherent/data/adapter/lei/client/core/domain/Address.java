package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;

/*
"LegalAddress": {
            "@xml:lang": "en",
            "FirstAddressLine": {
                "$": "207 Karyuu Bldg,872-7 Uedahara"
            },
            "City": {
                "$": "Ueda-city Nagano"
            },
            "Country": {
                "$": "JP"
            },
            "PostalCode": {
                "$": "386-1102"
            }
        }

        ALSO

        "HeadquartersAddress": {
            "@xml:lang": "en",
            "FirstAddressLine": {
                "$": "207 Karyuu Bldg,872-7 Uedahara"
            },
            "City": {
                "$": "Ueda-city Nagano"
            },
            "Country": {
                "$": "JP"
            },
            "PostalCode": {
                "$": "386-1102"
            }
        },

		ALSO

"OtherAddress": [{
                "@xml:lang": "ja",
                "@type": "ALTERNATIVE_LANGUAGE_LEGAL_ADDRESS",
                "FirstAddressLine": {
                    "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
                },
                "City": {
                    "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
                },
                "Country": {
                    "$": "JP"
                },
                "PostalCode": {
                    "$": "386-1102"
                }
            }
 */
public class Address implements XMLLangBeanSpecification {

    private String type;

    private String xmlLang;

    private String firstAddressLine;

    private String city;

    private String country;

    private String postalCode;

    public static final String
        TYPE = "type",
        TYPE_KEY = "@type",
        FIRST_ADDRESS_LINE = "firstAddressLine",
        CITY = "city",
        COUNTRY = "country",
        POSTAL_CODE = "postalCode";

    public String getType() {
        return type;
    }

    public void setType(@Changeable (TYPE) String type) {
        this.type = type;
    }

    @Override
    public void setXMLLang(@Changeable (XMLLangBeanSpecification.XMLLANG) String xmlLang) {
        this.xmlLang = xmlLang;
    }

    @Override
    public String getXMLLang() {
        return xmlLang;
    }

    public String getFirstAddressLine() {
        return firstAddressLine;
    }

    public void setFirstAddressLine(@Changeable (FIRST_ADDRESS_LINE) String firstAddressLine) {
        this.firstAddressLine = firstAddressLine;
    }

    public String getCity() {
        return city;
    }

    public void setCity(@Changeable (CITY) String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(@Changeable (COUNTRY) String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(@Changeable (POSTAL_CODE) String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + ((firstAddressLine == null) ? 0 : firstAddressLine.hashCode());
        result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((xmlLang == null) ? 0 : xmlLang.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Address other = (Address) obj;
        if (city == null) {
            if (other.city != null)
                return false;
        } else if (!city.equals(other.city))
            return false;
        if (country == null) {
            if (other.country != null)
                return false;
        } else if (!country.equals(other.country))
            return false;
        if (firstAddressLine == null) {
            if (other.firstAddressLine != null)
                return false;
        } else if (!firstAddressLine.equals(other.firstAddressLine))
            return false;
        if (postalCode == null) {
            if (other.postalCode != null)
                return false;
        } else if (!postalCode.equals(other.postalCode))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (xmlLang == null) {
            if (other.xmlLang != null)
                return false;
        } else if (!xmlLang.equals(other.xmlLang))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Address [type=" + type + ", xmlLang=" + xmlLang + ", firstAddressLine=" + firstAddressLine
            + ", city=" + city + ", country=" + country + ", postalCode=" + postalCode + ", toString()="
            + super.toString() + "]";
    }
}
