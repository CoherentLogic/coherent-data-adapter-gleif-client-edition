package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

public class LEIEntry extends SerializableBean {

    private static final long serialVersionUID = -5838569650378498579L;

    public static final String LEI = "lei", ENTITY = "entity";

    private LEI lei;

    private Entity entity;

    private Registration registration;

    public LEI getLei() {
        return lei;
    }

    public void setLei(@Changeable (LEI) LEI lei) {
        this.lei = lei;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(@Changeable (ENTITY) Entity entity) {
        this.entity = entity;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(@Changeable (Registration.REGISTRATION) Registration registration) {
        this.registration = registration;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((entity == null) ? 0 : entity.hashCode());
        result = prime * result + ((lei == null) ? 0 : lei.hashCode());
        result = prime * result + ((registration == null) ? 0 : registration.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LEIEntry other = (LEIEntry) obj;
        if (entity == null) {
            if (other.entity != null)
                return false;
        } else if (!entity.equals(other.entity))
            return false;
        if (lei == null) {
            if (other.lei != null)
                return false;
        } else if (!lei.equals(other.lei))
            return false;
        if (registration == null) {
            if (other.registration != null)
                return false;
        } else if (!registration.equals(other.registration))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "LEIEntry [lei=" + lei + ", entity=" + entity + ", registration=" + registration + "]";
    }
}
