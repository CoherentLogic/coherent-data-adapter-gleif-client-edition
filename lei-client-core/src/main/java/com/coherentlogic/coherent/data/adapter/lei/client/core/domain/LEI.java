package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

/*
"LEI": {
    "$": "35380063V116GJUGS786"
},
 */
public class LEI extends SerializableValueBean<String> {

    private static final long serialVersionUID = -180277091800102220L;

    public static final String LEI_KEY = "LEI";
}
