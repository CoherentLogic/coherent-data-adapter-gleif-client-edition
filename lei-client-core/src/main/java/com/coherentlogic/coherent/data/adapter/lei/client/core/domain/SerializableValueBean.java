package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

public abstract class SerializableValueBean<T> extends SerializableBean {

    private static final long serialVersionUID = 2702037573552015811L;

    public static final String KEY = "$";

    private T value;

    public T getValue() {
        return value;
    }

    public static final String VALUE = "value";

    public void setValue(@Changeable (VALUE) T value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SerializableValueBean other = (SerializableValueBean) obj;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SerializableValueBean [value=" + value + "]";
    }
}
