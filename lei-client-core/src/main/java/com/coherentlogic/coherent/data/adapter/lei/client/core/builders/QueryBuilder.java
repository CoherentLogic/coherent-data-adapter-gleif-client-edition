package com.coherentlogic.coherent.data.adapter.lei.client.core.builders;

import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.builders.rest.AbstractRESTQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.coherentlogic.coherent.data.adapter.core.util.WelcomeMessage;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse;

/**
 * API documentation: https://leilookup.gleif.org/docs/v2
 * 
 * https://www.gleif.org/content/2-about-lei/6-common-data-file-format/1-lei-cdf-format/2017-03-21_lei-cdf-v2-1.pdf
 * 
 * https://www.gleif.org/en/lei-data/gleif-concatenated-file/download-the-concatenated-file
 * 
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class QueryBuilder extends AbstractRESTQueryBuilder<String> {

    static final String[] WELCOME_MESSAGE = {
        " com.coherentlogic.lei.client:lei-client-core:3.0.0-RELEASE"
    };

    static {

        WelcomeMessage welcomeMessage = new WelcomeMessage();

        for (String next : WELCOME_MESSAGE) {
            welcomeMessage.addText(next);
        }

        welcomeMessage.display();
    }

    private static final Logger log = LoggerFactory.getLogger(QueryBuilder.class);

    public static final String DEFAULT_URI = "https://leilookup.gleif.org/api/v2";

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uri, cache, commandExecutor);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, DEFAULT_URI, cache);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uri, cache);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uri, commandExecutor);
    }

    public QueryBuilder(RestTemplate restTemplate) {
        super(restTemplate, DEFAULT_URI);
//        restTemplate.setInterceptors(Arrays.asList(new LoggingRequestInterceptor ()));
    }

    public QueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uriBuilder, cache, commandExecutor);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uriBuilder, cache);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uriBuilder, commandExecutor);
    }

    public QueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder) {
        super(restTemplate, uriBuilder);
    }

//    @Override
//    protected String getKey() {
//        return super.getEscapedURI();
//    }

    @Override
    public String getCacheKey () {
        return getEscapedURI();
    }

    static final String LEI_RECORDS = "leirecords";

    /**
     * Extends the path with "leirecords" as follows:
     *
     * https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786
     */
    public QueryBuilder leirecords () {
        return (QueryBuilder) extendPathWith(LEI_RECORDS);
    }

    static final String LEI = "lei";

    /**
     * The legal entity identifier, for example "35380063V116GJUGS786".
     */
    public QueryBuilder withLEI (String... leis) {
        return (QueryBuilder) addParameter (LEI, leis);
    }

    @Override
    protected <T> T doExecute(Class<T> type) {

        // Note that if we use the getUriBuilder().build().toASCIIString() method here the GLEIF web server
        // will kick back the request with a 400 error complaining that the lei string needs to be 0-20
        // chars and if you paste the same URL into a browser, or curl the same URL, data will be returned.
        //
        // The solution below, to use only etUriBuilder().build(), does work.
        //
        return (T) getRestTemplate ().getForObject(getUriBuilder().build(), type);
    }

    /**
     * Do get as {@link LEIResponse} and then return that result.
     */
    public LEIResponse doGetAsLEIResponse () {
        return doGetAsLEIResponse(leiResponse -> { return leiResponse; });
    }

    /**
     * Do get as {@link LEIResponse}, execute the given function, and then return an instance of type
     * {@link LEIResponse}.
     */
    public LEIResponse doGetAsLEIResponse (Function<LEIResponse, LEIResponse> function) {
        return doGetAsLEIResponse(LEIResponse.class, function);
    }

    /**
     * Do get as {@link LEIResponse}, execute the given function, and then return an instance of type R.
     */
    public <R> R doGetAsLEIResponse (Class<R> resultType, Function<LEIResponse, R> function) {

        LEIResponse identify = doGet(LEIResponse.class);

        R result = function.apply(identify);

        return result;
    }
}
