package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;

public interface XMLLangBeanSpecification {

    public static final String XMLLANG = "xmlLang", XML_LANG_KEY = "@xml:lang";

    void setXMLLang (@Changeable (XMLLANG) String xmlLang);

    String getXMLLang ();
}
