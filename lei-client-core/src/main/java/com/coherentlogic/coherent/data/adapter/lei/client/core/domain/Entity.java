package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
[{
    "LEI": {
        "$": "35380063V116GJUGS786"
    },
    "Entity": {
        "LegalName": {
            "@xml:lang": "ja",
            "$": "\u82b1\u5b9f\u5065\u98df\u682a\u5f0f\u4f1a\u793e"
        },
        "OtherEntityNames": {
            "OtherEntityName": [{
                "@xml:lang": "en",
                "@type": "ALTERNATIVE_LANGUAGE_LEGAL_NAME",
                "$": "TUBOMIKENSHOKU CO.,LTD"
            }]
        },
        "LegalAddress": {
            "@xml:lang": "en",
            "FirstAddressLine": {
                "$": "207 Karyuu Bldg,872-7 Uedahara"
            },
            "City": {
                "$": "Ueda-city Nagano"
            },
            "Country": {
                "$": "JP"
            },
            "PostalCode": {
                "$": "386-1102"
            }
        },
        "HeadquartersAddress": {
            "@xml:lang": "en",
            "FirstAddressLine": {
                "$": "207 Karyuu Bldg,872-7 Uedahara"
            },
            "City": {
                "$": "Ueda-city Nagano"
            },
            "Country": {
                "$": "JP"
            },
            "PostalCode": {
                "$": "386-1102"
            }
        },
        "OtherAddresses": {
            "OtherAddress": [{
                "@xml:lang": "ja",
                "@type": "ALTERNATIVE_LANGUAGE_LEGAL_ADDRESS",
                "FirstAddressLine": {
                    "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
                },
                "City": {
                    "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
                },
                "Country": {
                    "$": "JP"
                },
                "PostalCode": {
                    "$": "386-1102"
                }
            }, {
                "@xml:lang": "ja",
                "@type": "ALTERNATIVE_LANGUAGE_HEADQUARTERS_ADDRESS",
                "FirstAddressLine": {
                    "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
                },
                "City": {
                    "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
                },
                "Country": {
                    "$": "JP"
                },
                "PostalCode": {
                    "$": "386-1102"
                }
            }]
        },
        "RegistrationAuthority": {
            "RegistrationAuthorityID": {
                "$": "RA000412"
            },
            "RegistrationAuthorityEntityID": {
                "$": "100001026624"
            }
        },
        "LegalJurisdiction": {
            "$": "JP"
        },
        "EntityStatus": {
            "$": "ACTIVE"
        }
    },
    "Registration": {
        "InitialRegistrationDate": {
            "$": "2018-07-12T06:00:02+00:00"
        },
        "LastUpdateDate": {
            "$": "2018-07-12T06:00:02+00:00"
        },
        "RegistrationStatus": {
            "$": "ISSUED"
        },
        "NextRenewalDate": {
            "$": "2019-07-12T06:00:02+00:00"
        },
        "ManagingLOU": {
            "$": "353800279ADEFGKNTV65"
        },
        "ValidationSources": {
            "$": "FULLY_CORROBORATED"
        }
    }
}]
 *
 * https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786
 *
 * https://www.gleif.org/en/lei-data/gleif-concatenated-file/download-the-concatenated-file
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 */
public class Entity extends SerializableBean {

    private static final long serialVersionUID = -142779133626641303L;

    private LegalName legalName;

    private Address legalAddress, headquartersAddress;

    private List<Address> otherAddressList = new ArrayList<Address> ();

    private RegistrationAuthority registrationAuthority;

    public static final String ENTITY_KEY = "Entity";

    public static final String LEGAL_JURISDICTION_KEY = "LegalJurisdiction",
        ENTITY_STATUS_KEY = "EntityStatus";

    public static final String LEGAL_JURISDICTION = "legalJurisdiction",
        ENTITY_STATUS = "entityStatus";

    private String legalJurisdiction;

    private String entityStatus;

    private Registration registration;

    public LegalName getLegalName() {
        return legalName;
    }

    public static final String LEGALNAME = "legalName", LEGAL_ADDRESS = "legalAddress",
        HEADQUARTERS_ADDRESS = "headquartersAddress", OTHER_ADDRESS_LIST = "otherAddressList";

    public void setLegalName(@Changeable (LEGALNAME) LegalName legalName) {
        this.legalName = legalName;
    }

    private OtherEntityNames otherEntityNames;

    public OtherEntityNames getOtherEntityNames() {
        return otherEntityNames;
    }

    public static final String OTHER_ENTITY_NAMES = "otherEntityNames";

    public void setOtherEntityNames(@Changeable (OTHER_ENTITY_NAMES) OtherEntityNames otherEntityNames) {
        this.otherEntityNames = otherEntityNames;
    }

    public Address getLegalAddress() {
        return legalAddress;
    }

    public void setLegalAddress(@Changeable (LEGAL_ADDRESS) Address legalAddress) {
        this.legalAddress = legalAddress;
    }

    public Address getHeadquartersAddress() {
        return headquartersAddress;
    }

    public void setHeadquartersAddress(@Changeable (HEADQUARTERS_ADDRESS) Address headquartersAddress) {
        this.headquartersAddress = headquartersAddress;
    }

    public List<Address> getOtherAddressList() {
        return otherAddressList;
    }

    public void setOtherAddressList(@Changeable (OTHER_ADDRESS_LIST) List<Address> otherAddressList) {
        this.otherAddressList = otherAddressList;
    }

    public void addOtherAddress(Address... address) {
        addOtherAddress (Arrays.asList(address));
    }

    public void addOtherAddress(List<Address> otherAddressList) {
        this.otherAddressList.addAll(otherAddressList);
    }

    /**
     * Delegates to the otherAddressList.forEach method.
     */
    public void forEachOtherAddress (Consumer<? super Address> consumer) {
        this.otherAddressList.forEach(consumer);
    }

    public RegistrationAuthority getRegistrationAuthority() {
        return registrationAuthority;
    }

    public void setRegistrationAuthority(
        @Changeable (RegistrationAuthority.REGISTRATION_AUTHORITY_KEY)
            RegistrationAuthority registrationAuthority
    ) {
        this.registrationAuthority = registrationAuthority;
    }

    public String getLegalJurisdiction() {
        return legalJurisdiction;
    }

    public void setLegalJurisdiction(@Changeable (LEGAL_JURISDICTION) String legalJurisdiction) {
        this.legalJurisdiction = legalJurisdiction;
    }

    public String getEntityStatus() {
        return entityStatus;
    }

    public void setEntityStatus(@Changeable (ENTITY_STATUS) String entityStatus) {
        this.entityStatus = entityStatus;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(@Changeable (Registration.REGISTRATION) Registration registration) {
        this.registration = registration;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((entityStatus == null) ? 0 : entityStatus.hashCode());
        result = prime * result + ((headquartersAddress == null) ? 0 : headquartersAddress.hashCode());
        result = prime * result + ((legalAddress == null) ? 0 : legalAddress.hashCode());
        result = prime * result + ((legalJurisdiction == null) ? 0 : legalJurisdiction.hashCode());
        result = prime * result + ((legalName == null) ? 0 : legalName.hashCode());
        result = prime * result + ((otherAddressList == null) ? 0 : otherAddressList.hashCode());
        result = prime * result + ((otherEntityNames == null) ? 0 : otherEntityNames.hashCode());
        result = prime * result + ((registration == null) ? 0 : registration.hashCode());
        result = prime * result + ((registrationAuthority == null) ? 0 : registrationAuthority.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Entity other = (Entity) obj;
        if (entityStatus == null) {
            if (other.entityStatus != null)
                return false;
        } else if (!entityStatus.equals(other.entityStatus))
            return false;
        if (headquartersAddress == null) {
            if (other.headquartersAddress != null)
                return false;
        } else if (!headquartersAddress.equals(other.headquartersAddress))
            return false;
        if (legalAddress == null) {
            if (other.legalAddress != null)
                return false;
        } else if (!legalAddress.equals(other.legalAddress))
            return false;
        if (legalJurisdiction == null) {
            if (other.legalJurisdiction != null)
                return false;
        } else if (!legalJurisdiction.equals(other.legalJurisdiction))
            return false;
        if (legalName == null) {
            if (other.legalName != null)
                return false;
        } else if (!legalName.equals(other.legalName))
            return false;
        if (otherAddressList == null) {
            if (other.otherAddressList != null)
                return false;
        } else if (!otherAddressList.equals(other.otherAddressList))
            return false;
        if (otherEntityNames == null) {
            if (other.otherEntityNames != null)
                return false;
        } else if (!otherEntityNames.equals(other.otherEntityNames))
            return false;
        if (registration == null) {
            if (other.registration != null)
                return false;
        } else if (!registration.equals(other.registration))
            return false;
        if (registrationAuthority == null) {
            if (other.registrationAuthority != null)
                return false;
        } else if (!registrationAuthority.equals(other.registrationAuthority))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Entity [legalName=" + legalName + ", legalAddress=" + legalAddress + ", headquartersAddress="
            + headquartersAddress + ", otherAddressList=" + otherAddressList + ", registrationAuthority="
            + registrationAuthority + ", legalJurisdiction=" + legalJurisdiction + ", entityStatus="
            + entityStatus + ", registration=" + registration + ", otherEntityNames=" + otherEntityNames
            + "]";
    }
}
