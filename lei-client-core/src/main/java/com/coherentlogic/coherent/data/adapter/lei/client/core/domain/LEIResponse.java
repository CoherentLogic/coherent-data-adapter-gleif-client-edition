package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
[{
    "LEI": {
        "$": "35380063V116GJUGS786"
    },
    "Entity": {
        "LegalName": {
            "@xml:lang": "ja",
            "$": "\u82b1\u5b9f\u5065\u98df\u682a\u5f0f\u4f1a\u793e"
        },
        "OtherEntityNames": {
            "OtherEntityName": [{
                "@xml:lang": "en",
                "@type": "ALTERNATIVE_LANGUAGE_LEGAL_NAME",
                "$": "TUBOMIKENSHOKU CO.,LTD"
            }]
        },
        "LegalAddress": {
            "@xml:lang": "en",
            "FirstAddressLine": {
                "$": "207 Karyuu Bldg,872-7 Uedahara"
            },
            "City": {
                "$": "Ueda-city Nagano"
            },
            "Country": {
                "$": "JP"
            },
            "PostalCode": {
                "$": "386-1102"
            }
        },
        "HeadquartersAddress": {
            "@xml:lang": "en",
            "FirstAddressLine": {
                "$": "207 Karyuu Bldg,872-7 Uedahara"
            },
            "City": {
                "$": "Ueda-city Nagano"
            },
            "Country": {
                "$": "JP"
            },
            "PostalCode": {
                "$": "386-1102"
            }
        },
        "OtherAddresses": {
            "OtherAddress": [{
                "@xml:lang": "ja",
                "@type": "ALTERNATIVE_LANGUAGE_LEGAL_ADDRESS",
                "FirstAddressLine": {
                    "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
                },
                "City": {
                    "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
                },
                "Country": {
                    "$": "JP"
                },
                "PostalCode": {
                    "$": "386-1102"
                }
            }, {
                "@xml:lang": "ja",
                "@type": "ALTERNATIVE_LANGUAGE_HEADQUARTERS_ADDRESS",
                "FirstAddressLine": {
                    "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
                },
                "City": {
                    "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
                },
                "Country": {
                    "$": "JP"
                },
                "PostalCode": {
                    "$": "386-1102"
                }
            }]
        },
        "RegistrationAuthority": {
            "RegistrationAuthorityID": {
                "$": "RA000412"
            },
            "RegistrationAuthorityEntityID": {
                "$": "100001026624"
            }
        },
        "LegalJurisdiction": {
            "$": "JP"
        },
        "EntityStatus": {
            "$": "ACTIVE"
        }
    },
    "Registration": {
        "InitialRegistrationDate": {
            "$": "2018-07-12T06:00:02+00:00"
        },
        "LastUpdateDate": {
            "$": "2018-07-12T06:00:02+00:00"
        },
        "RegistrationStatus": {
            "$": "ISSUED"
        },
        "NextRenewalDate": {
            "$": "2019-07-12T06:00:02+00:00"
        },
        "ManagingLOU": {
            "$": "353800279ADEFGKNTV65"
        },
        "ValidationSources": {
            "$": "FULLY_CORROBORATED"
        }
    }
}]
 *
 * https://leilookup.gleif.org/api/v2/leirecords?lei=35380063V116GJUGS786
 *
 * https://www.gleif.org/en/lei-data/gleif-concatenated-file/download-the-concatenated-file
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 */
public class LEIResponse extends SerializableBean {

    private static final long serialVersionUID = -8285600547087717312L;

    public static final String LEI_ENTRY_LIST = "leiEntryList";

    private List<LEIEntry> leiEntryList = new ArrayList<LEIEntry> ();

    public List<LEIEntry> getLeiEntryList() {
        return leiEntryList;
    }

    public void setLeiEntryList(@Changeable (LEI_ENTRY_LIST) List<LEIEntry> leiEntryList) {
        this.leiEntryList = leiEntryList;
    }

    public void addLEIEntry (LEIEntry leiEntry) {
        this.leiEntryList.add(leiEntry);
    }

    /**
     * Delegate method to the {@link #leiEntryList} forEach method.
     */
    public void forEachLEIEntry (Consumer<? super LEIEntry> consumer) {
        this.leiEntryList.forEach(consumer);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((leiEntryList == null) ? 0 : leiEntryList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LEIResponse other = (LEIResponse) obj;
        if (leiEntryList == null) {
            if (other.leiEntryList != null)
                return false;
        } else if (!leiEntryList.equals(other.leiEntryList))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "LEIResponse [leiEntryList=" + leiEntryList + "]";
    }
}
