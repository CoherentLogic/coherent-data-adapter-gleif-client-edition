package com.coherentlogic.coherent.data.adapter.lei.client.core.converters;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.coherentlogic.coherent.data.adapter.core.exceptions.ConversionFailedException;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.Address;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.Entity;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEI;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIEntry;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LEIResponse;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.LegalName;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.OtherEntityName;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.OtherEntityNames;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.Registration;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.RegistrationAuthority;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.SerializableValueBean;
import com.coherentlogic.coherent.data.adapter.lei.client.core.domain.XMLLangBeanSpecification;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * 
[{
    "LEI": {
        "$": "35380063V116GJUGS786"
    },
    "Entity": {
        "LegalName": {
            "@xml:lang": "ja",
            "$": "\u82b1\u5b9f\u5065\u98df\u682a\u5f0f\u4f1a\u793e"
        },
        "OtherEntityNames": {
            "OtherEntityName": [{
                "@xml:lang": "en",
                "@type": "ALTERNATIVE_LANGUAGE_LEGAL_NAME",
                "$": "TUBOMIKENSHOKU CO.,LTD"
            }]
        },
        "LegalAddress": {
            "@xml:lang": "en",
            "FirstAddressLine": {
                "$": "207 Karyuu Bldg,872-7 Uedahara"
            },
            "City": {
                "$": "Ueda-city Nagano"
            },
            "Country": {
                "$": "JP"
            },
            "PostalCode": {
                "$": "386-1102"
            }
        },
        "HeadquartersAddress": {
            "@xml:lang": "en",
            "FirstAddressLine": {
                "$": "207 Karyuu Bldg,872-7 Uedahara"
            },
            "City": {
                "$": "Ueda-city Nagano"
            },
            "Country": {
                "$": "JP"
            },
            "PostalCode": {
                "$": "386-1102"
            }
        },
        "OtherAddresses": {
            "OtherAddress": [{
                "@xml:lang": "ja",
                "@type": "ALTERNATIVE_LANGUAGE_LEGAL_ADDRESS",
                "FirstAddressLine": {
                    "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
                },
                "City": {
                    "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
                },
                "Country": {
                    "$": "JP"
                },
                "PostalCode": {
                    "$": "386-1102"
                }
            }, {
                "@xml:lang": "ja",
                "@type": "ALTERNATIVE_LANGUAGE_HEADQUARTERS_ADDRESS",
                "FirstAddressLine": {
                    "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
                },
                "City": {
                    "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
                },
                "Country": {
                    "$": "JP"
                },
                "PostalCode": {
                    "$": "386-1102"
                }
            }]
        },
        "RegistrationAuthority": {
            "RegistrationAuthorityID": {
                "$": "RA000412"
            },
            "RegistrationAuthorityEntityID": {
                "$": "100001026624"
            }
        },
        "LegalJurisdiction": {
            "$": "JP"
        },
        "EntityStatus": {
            "$": "ACTIVE"
        }
    },
    "Registration": {
        "InitialRegistrationDate": {
            "$": "2018-07-12T06:00:02+00:00"
        },
        "LastUpdateDate": {
            "$": "2018-07-12T06:00:02+00:00"
        },
        "RegistrationStatus": {
            "$": "ISSUED"
        },
        "NextRenewalDate": {
            "$": "2019-07-12T06:00:02+00:00"
        },
        "ManagingLOU": {
            "$": "353800279ADEFGKNTV65"
        },
        "ValidationSources": {
            "$": "FULLY_CORROBORATED"
        }
    }
}]
 *
 */
public class LEIResponseDeserializer implements JsonDeserializer<LEIResponse> {

    private static final Logger log = LoggerFactory.getLogger(LEIResponseDeserializer.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public LEIResponse deserialize(JsonElement element, Type type, JsonDeserializationContext context)
        throws JsonParseException {

        log.debug("deserialize: method begins; element: " + element + ", type: " + type + ", context: "
            + context);

        LEIResponse result = null;

        if (applicationContext != null) {

            log.debug("The LEIResponse instance will be acquired from the non-null applicationContext.");

            result = applicationContext.getBean(LEIResponse.class);
        } else {

            log.debug("The LEIResponse instance will be instantiated directly since the applicationContext is null.");

            result = new LEIResponse ();
        }

        JsonArray leiEntryArray = element.getAsJsonArray();

        for (JsonElement nextLEIEntryElement : leiEntryArray) {

            if (nextLEIEntryElement.isJsonObject()) {

                JsonObject nextLEIEntryObject = nextLEIEntryElement.getAsJsonObject();

                JsonElement nextLEIElement = nextLEIEntryObject.get("LEI");

                LEI lei = toLEI (nextLEIElement);

                JsonElement nextEntity = nextLEIEntryObject.get(Entity.ENTITY_KEY);

                log.debug("nextEntity: " + nextEntity);

                Entity entity = toEntity(nextEntity);

                Registration registration =
                    toRegistration (nextLEIEntryObject.get(Registration.REGISTRATION));

                LEIEntry leiEntry = new LEIEntry ();

                leiEntry.setLei(lei);
                leiEntry.setEntity(entity);
                leiEntry.setRegistration(registration);

                result.addLEIEntry(leiEntry);

            } else
                throw new ConversionFailedException ("Expected an object for the nextLEIEntry; "
                    + "nextLEIEntry: " + nextLEIEntryElement);
        }

        log.debug("deserialize: method ends; result: " + result);

        return result;
    }

    Registration toRegistration (JsonElement registrationJsonElement) {

        try {
            return toRegistration (registrationJsonElement.getAsJsonObject());
        } catch (ParseException parseException) {
            throw new ConversionFailedException("Conversion failed -- refer to the cause for details.",
                parseException);
        }
    }

    /**
     * // 2018-07-12T06:00:02+00:00
     * def dateText = "2018-07-12T06:00:02+00:00"
     *
     * //
     * def dateFormat = new java.text.SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ssX")
     *
     * def date = dateFormat.parse (dateText)
     *
     * println ("date: $date")
     */
    static DateFormat dateFormatWithMillis = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss.SSSX");

    static DateFormat dateFormat3Sec = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:sssX");

    /**
     * TODO: Improve this method as there should be a better solution that what appears below.
     */
    static Date toDate (String dateText) {

        Date result = null;

        try {
            result = dateFormatWithMillis.parse(dateText);
        } catch (ParseException e) {
            try {
                result = dateFormat3Sec.parse(dateText);
            } catch (ParseException e1) {
                throw new ConversionFailedException("Unable to parse the dateText: " + dateText
                    + " using the patterns: dateFormatWithMillis: " + dateFormatWithMillis + " and "
                    + dateFormat3Sec, e1);
            }
        }
        return result;
    }

    /**
     * "Registration": {
     *     "InitialRegistrationDate": {
     *         "$": "2018-07-12T06:00:02+00:00"
     *     },
     *     "LastUpdateDate": {
     *         "$": "2018-07-12T06:00:02+00:00"
     *     },
     *     "RegistrationStatus": {
     *         "$": "ISSUED"
     *     },
     *     "NextRenewalDate": {
     *         "$": "2019-07-12T06:00:02+00:00"
     *     },
     *     "ManagingLOU": {
     *         "$": "353800279ADEFGKNTV65"
     *     },
     *     "ValidationSources": {
     *         "$": "FULLY_CORROBORATED"
     *     }
     * } 
     */
    Registration toRegistration (JsonObject registrationJsonObject) throws ParseException {

        Registration result = new Registration ();

        String initialRegistrationDateText =
            getOptionalValueString(Registration.INITIAL_REGISTRATION_DATE_KEY, registrationJsonObject);
        String lastUpdateDateText =
            getOptionalValueString(Registration.LAST_UPDATE_DATE_KEY, registrationJsonObject);
        String registrationStatus =
            getOptionalValueString(Registration.REGISTRATION_STATUS_KEY, registrationJsonObject);
        String nextRenewalDateText =
            getOptionalValueString(Registration.NEXT_RENEWAL_DATE_KEY, registrationJsonObject);
        String managingLOU =
            getOptionalValueString(Registration.MANAGING_LOU_KEY, registrationJsonObject);
        String validationSources =
            getOptionalValueString(Registration.VALIDATION_SOURCES_KEY, registrationJsonObject);

        Date initialRegistrationDate =
            initialRegistrationDateText == null ? null : toDate (initialRegistrationDateText);

        result.setInitialRegistrationDate(initialRegistrationDate);

        Date lastUpdateDate = lastUpdateDateText == null ? null : toDate (lastUpdateDateText);

        result.setLastUpdateDate (lastUpdateDate);

        result.setRegistrationStatus (registrationStatus);

        Date nextRenewalDate = lastUpdateDateText == null ? null : toDate (nextRenewalDateText);

        result.setNextRenewalDate (nextRenewalDate);
        result.setManagingLOU (managingLOU);
        result.setValidationSources (validationSources);

        return result;
    }

    LEI toLEI (JsonElement leiJsonElement) {

        if (leiJsonElement.isJsonObject()) {

            JsonObject leiObject = leiJsonElement.getAsJsonObject();

            return toLEI (leiObject);

        } else
            throw new ConversionFailedException ("Expected an object for the leiElement; "
                + "leiJsonElement: " + leiJsonElement);
    }

    LEI toLEI (JsonObject leiObject) {

        LEI lei = new LEI ();

        String value = leiObject.get(SerializableValueBean.KEY).getAsString();

        lei.setValue(value);

        return lei;
    }

    Entity toEntity (JsonElement entityJsonElement) {

        if (entityJsonElement.isJsonObject())
            return toEntity (entityJsonElement.getAsJsonObject());
        else
            throw new ConversionFailedException ("Expected an object for the entityJsonElement; "
                    + "entityJsonElement: " + entityJsonElement);
    }

    Entity toEntity (JsonObject entityJsonObject) {

        log.debug("toEntity: method begins; entityJsonObject: " + entityJsonObject);

        Entity result = new Entity ();

        JsonObject legalNameJsonObject = entityJsonObject.get("LegalName").getAsJsonObject();

        log.debug("entityJsonObject: " + entityJsonObject + ", legalNameJsonObject: " + legalNameJsonObject);

        LegalName legalName = toLegalName (legalNameJsonObject);

        result.setLegalName(legalName);

        if (entityJsonObject.has("OtherEntityNames")) {

            JsonObject otherEntityNamesJsonObject =
                entityJsonObject.get("OtherEntityNames").getAsJsonObject();

            log.debug("otherEntityNamesJsonObject: " + otherEntityNamesJsonObject);

            OtherEntityNames otherEntityNames = toOtherEntityNames (otherEntityNamesJsonObject);

            result.setOtherEntityNames(otherEntityNames);
        }

        result.setLegalAddress(getOptionalAddress(entityJsonObject, LEGAL_ADDRESS_KEY));
        result.setHeadquartersAddress(getOptionalAddress(entityJsonObject, HEADQUARTERS_ADDRESS_KEY));
        result.addOtherAddress(getOptionalOtherAddresses (entityJsonObject));
        result.setRegistrationAuthority(getOptionalRegistrationAuthority (entityJsonObject));

        result.setLegalJurisdiction(getOptionalValueString(Entity.LEGAL_JURISDICTION_KEY, entityJsonObject));

        result.setEntityStatus(getOptionalValueString(Entity.ENTITY_STATUS_KEY, entityJsonObject));

        log.debug("toEntity: method ends; result: " + result);

        return result;
    }

    /**
     * "RegistrationAuthority": {
     *     "RegistrationAuthorityID": {
     *         "$": "RA000412"
     *     },
     *     "RegistrationAuthorityEntityID": {
     *         "$": "100001026624"
     *      }
     *  }
     */
    RegistrationAuthority getOptionalRegistrationAuthority (JsonObject entityJsonObject) {

        log.debug("getOptionalRegistrationAuthority: method begins; entityJsonObject: " + entityJsonObject);

        RegistrationAuthority result = null;

        if (entityJsonObject.has(RegistrationAuthority.REGISTRATION_AUTHORITY_KEY)) {

            result = new RegistrationAuthority ();

            JsonObject registrationAuthorityJsonObject =
                entityJsonObject.getAsJsonObject(RegistrationAuthority.REGISTRATION_AUTHORITY_KEY);

            result.setRegistrationAuthorityID(
                getOptionalValueString(
                    RegistrationAuthority.REGISTRATION_AUTHORITY_ID_KEY, registrationAuthorityJsonObject
                )
            );

            result.setRegistrationAuthorityEntityID(
                getOptionalValueString(
                    RegistrationAuthority.REGISTRATION_AUTHORITY_ENTITY_ID_KEY,
                    registrationAuthorityJsonObject
                )
            );
        }

        log.debug("getOptionalRegistrationAuthority: method ends; result: " + result);

        return result;
    }

    final String LEGAL_ADDRESS_KEY = "LegalAddress", HEADQUARTERS_ADDRESS_KEY = "HeadquartersAddress",
        OTHER_ADDRESSES = "OtherAddresses", OTHER_ADDRESS = "OtherAddress";

    /**
     *
     * "OtherAddresses": {
     *     "OtherAddress": [{
     *         "@xml:lang": "ja",
     *         "@type": "ALTERNATIVE_LANGUAGE_LEGAL_ADDRESS",
     *         "FirstAddressLine": {
     *             "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
     *         },
     *         "City": {
     *             "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
     *         },
     *         "Country": {
     *             "$": "JP"
     *         },
     *         "PostalCode": {
     *             "$": "386-1102"
     *         }
     *     }, {
     *         "@xml:lang": "ja",
     *         "@type": "ALTERNATIVE_LANGUAGE_HEADQUARTERS_ADDRESS",
     *         "FirstAddressLine": {
     *             "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
     *         },
     *         "City": {
     *             "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
     *         },
     *         "Country": {
     *             "$": "JP"
     *         },
     *         "PostalCode": {
     *             "$": "386-1102"
     *         }
     *     }]
     * }
     */
    List<Address> getOptionalOtherAddresses (JsonObject entityJsonObject) {

        final List<Address> result = new ArrayList<Address> ();

        if (entityJsonObject.has(OTHER_ADDRESSES)) {

            JsonObject otherAddresssesJsonObject = entityJsonObject.get(OTHER_ADDRESSES).getAsJsonObject();

            if (otherAddresssesJsonObject.has(OTHER_ADDRESS)) {

                JsonArray otherAddressJsonArray = otherAddresssesJsonObject.getAsJsonArray(OTHER_ADDRESS);

                otherAddressJsonArray.forEach(addressJsonElement -> {
                    result.add(toAddress(addressJsonElement, OTHER_ADDRESS));
                });
            }
        }

        return result;
    }

    LegalName toLegalName (JsonObject legalNameJsonObject) {

        LegalName result = new LegalName ();

        if (legalNameJsonObject.has(XMLLangBeanSpecification.XML_LANG_KEY)) {

            String xmlLang = legalNameJsonObject.get(XMLLangBeanSpecification.XML_LANG_KEY).getAsString();

            result.setXMLLang(xmlLang);
        }

        String value = legalNameJsonObject.get(SerializableValueBean.KEY).getAsString();

        result.setValue(value);

        return result;
    }

    /**
     * "OtherEntityNames": {
     *     "OtherEntityName": [{
     *         "@xml:lang": "en",
     *         "@type": "ALTERNATIVE_LANGUAGE_LEGAL_NAME",
     *         "$": "TUBOMIKENSHOKU CO.,LTD"
     *     }]
     * },
     */
    OtherEntityNames toOtherEntityNames (JsonElement otherEntityNamesJsonElement) {

        if (otherEntityNamesJsonElement.isJsonObject())
            return toOtherEntityNames (otherEntityNamesJsonElement.getAsJsonObject());
        else
            throw new ConversionFailedException ("Expected an object for the entityJsonElement; "
                + "otherEntityNamesJsonElement: " + otherEntityNamesJsonElement);
    }

    /**
     * "OtherEntityNames": {
     *     "OtherEntityName": [{
     *         "@xml:lang": "en",
     *         "@type": "ALTERNATIVE_LANGUAGE_LEGAL_NAME",
     *         "$": "TUBOMIKENSHOKU CO.,LTD"
     *     }]
     * },
     */
    OtherEntityNames toOtherEntityNames (JsonObject otherEntityNamesJsonObject) {

        OtherEntityNames result = new OtherEntityNames ();

        JsonArray otherEntityNameJsonArray =
            otherEntityNamesJsonObject.get(OtherEntityName.OTHER_ENTITY_NAME_KEY).getAsJsonArray();

        List<OtherEntityName> otherEntityNames = toOtherEntityName (otherEntityNameJsonArray);

        result.addOtherEntityNames(otherEntityNames);

        return result;
    }

    List<OtherEntityName> toOtherEntityName (JsonArray otherEntityNameJsonArray) {

        List<OtherEntityName> result = new ArrayList<OtherEntityName> (otherEntityNameJsonArray.size());

        otherEntityNameJsonArray.forEach(
            otherEntityNameElement -> {
                result.add(toOtherEntityName (otherEntityNameElement.getAsJsonObject()));
            }
        );

        return result;
    }

    /**
     * "OtherEntityNames": {
     *     "OtherEntityName": [{
     *         "@xml:lang": "en",
     *         "@type": "ALTERNATIVE_LANGUAGE_LEGAL_NAME",
     *         "$": "TUBOMIKENSHOKU CO.,LTD"
     *     }]
     * },
     */
    OtherEntityName toOtherEntityName (JsonObject otherEntityNameJsonObject) {

        OtherEntityName result = new OtherEntityName ();

        String xmlLang = otherEntityNameJsonObject.get(XMLLangBeanSpecification.XML_LANG_KEY).getAsString();
        String type = otherEntityNameJsonObject.get(OtherEntityName.TYPE_KEY).getAsString();
        String value = otherEntityNameJsonObject.get(SerializableValueBean.KEY).getAsString();

        result.setXMLLang(xmlLang);
        result.setType(type);
        result.setValue(value);

        return result;
    }

    /**
     * "LegalAddress": {
     *     "@xml:lang": "en",
     *     "FirstAddressLine": {
     *         "$": "207 Karyuu Bldg,872-7 Uedahara"
     *     },
     *     "City": {
     *         "$": "Ueda-city Nagano"
     *     },
     *     "Country": {
     *         "$": "JP"
     *     },
     *     "PostalCode": {
     *         "$": "386-1102"
     *     }
     * }
     */
    Address getOptionalAddress (JsonObject entityJsonObject, String targetAddressKey) {

        Address result = null;

        if (entityJsonObject.has(targetAddressKey)) {

            JsonElement addressJsonElement = entityJsonObject.get(targetAddressKey);

            result = toAddress(addressJsonElement, targetAddressKey);
        }

        return result;
    }

    /**
     * 
     * @param addressJsonElement
     * @param targetAddressKey LegalAddress or HeadquartersAddress, etc
     * @return
     */
    Address toAddress (JsonElement addressJsonElement, String targetAddressKey) {

        if (addressJsonElement.isJsonObject())
            return toAddress (addressJsonElement.getAsJsonObject());
        else
            throw new ConversionFailedException ("Expected an object for the legalAddressJsonElement; "
                + "legalAddressJsonElement: " + addressJsonElement + " and targetAddressKey: "
                + targetAddressKey);
    }

    public static final String FIRST_ADDRESS_LINE = "FirstAddressLine",
        CITY = "City",
        COUNTRY = "Country",
        POSTAL_CODE = "PostalCode";


    /**
     * "OtherAddress": [{
     *     "@xml:lang": "ja",
     *     "@type": "ALTERNATIVE_LANGUAGE_LEGAL_ADDRESS",
     *     "FirstAddressLine": {
     *         "$": "\u4e0a\u7530\u539f872-7\u83ef\u9f8d\u30d3\u30eb207"
     *     },
     *     "City": {
     *         "$": "\u9577\u91ce\u770c \u4e0a\u7530\u5e02"
     *     },
     *     "Country": {
     *         "$": "JP"
     *     },
     *     "PostalCode": {
     *         "$": "386-1102"
     *     }
     * }
     * ...
     * ]
     */
    Address toAddress (JsonObject addressJsonObject) {

        log.debug("toAddress: method begins; addressJsonObject: " + addressJsonObject);

        Address result = new Address ();

        if (addressJsonObject.has(XMLLangBeanSpecification.XML_LANG_KEY)) {

            String xmlLang = addressJsonObject.get(XMLLangBeanSpecification.XML_LANG_KEY).getAsString();

            result.setXMLLang(xmlLang);
        }

        if (addressJsonObject.has(Address.TYPE_KEY)) {

            String type = addressJsonObject.get(Address.TYPE_KEY).getAsString();

            result.setType(type);
        }

        String firstAddressLine = getOptionalValueString (FIRST_ADDRESS_LINE, addressJsonObject);

        result.setFirstAddressLine(firstAddressLine);

        String city = getOptionalValueString (CITY, addressJsonObject);

        result.setCity(city);

        String country = getOptionalValueString (COUNTRY, addressJsonObject);

        result.setCountry(country);

        String postalCode = getOptionalValueString (POSTAL_CODE, addressJsonObject);

        result.setPostalCode(postalCode);

        log.debug("toLegalAddress: method ends; result: " + result);

        return result;
    }

    String getOptionalValueString (String key, JsonObject jsonObject) {

        log.debug("getOptionalValueString: method begins; key: " + key + ", jsonObject: " + jsonObject);

        String result = null;

        if (jsonObject.has(key))
            result = jsonObject.get(key).getAsJsonObject().get(SerializableValueBean.KEY).getAsString();

        log.debug("getOptionalValueString: method ends; result: " + result);

        return result;
    }
}
