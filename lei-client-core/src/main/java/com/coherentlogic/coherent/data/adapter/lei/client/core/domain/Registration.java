package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import java.util.Date;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
"Registration": {
    "InitialRegistrationDate": {
        "$": "2018-07-12T06:00:02+00:00"
    },
    "LastUpdateDate": {
        "$": "2018-07-12T06:00:02+00:00"
    },
    "RegistrationStatus": {
        "$": "ISSUED"
    },
    "NextRenewalDate": {
        "$": "2019-07-12T06:00:02+00:00"
    },
    "ManagingLOU": {
        "$": "353800279ADEFGKNTV65"
    },
    "ValidationSources": {
        "$": "FULLY_CORROBORATED"
    }
}

-----

// 2018-07-12T06:00:02+00:00
def dateText = "2018-07-12T06:00:02+00:00"

//
def dateFormat = new java.text.SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ssX")

def date = dateFormat.parse (dateText)

println ("date: $date")

 */
public class Registration extends SerializableBean {

    private static final long serialVersionUID = -3729617839793977884L;

    public static final String REGISTRATION_KEY = "Registration", REGISTRATION = REGISTRATION_KEY;

    public static final String INITIAL_REGISTRATION_DATE_KEY = "InitialRegistrationDate",
        LAST_UPDATE_DATE_KEY = "LastUpdateDate",
        REGISTRATION_STATUS_KEY = "RegistrationStatus",
        NEXT_RENEWAL_DATE_KEY = "NextRenewalDate",
        MANAGING_LOU_KEY = "ManagingLOU",
        VALIDATION_SOURCES_KEY = "ValidationSources";

    public static final String INITIAL_REGISTRATION_DATE = "initialRegistrationDate",
        LAST_UPDATE_DATE = "lastUpdateDate",
        REGISTRATION_STATUS = "registrationStatus",
        NEXT_RENEWAL_DATE = "nextRenewalDate",
        MANAGING_LOU = "managingLOU",
        VALIDATION_SOURCES = "validationSources";

    private Date initialRegistrationDate;

    private Date lastUpdateDate;

    /**
     * @TODO Convert to an enum?
     */
    private String registrationStatus;

    private Date nextRenewalDate;

    private String managingLOU;

    private String validationSources;

    public java.sql.Date getInitialRegistrationDateAsSQLDate() {
        return initialRegistrationDate == null
            ? null : new java.sql.Date (initialRegistrationDate.getTime());
    }

    public Date getInitialRegistrationDate() {
        return initialRegistrationDate;
    }

    public void setInitialRegistrationDate(
        @Changeable (INITIAL_REGISTRATION_DATE) Date initialRegistrationDate
    ) {
        this.initialRegistrationDate = initialRegistrationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public java.sql.Date getLastUpdateDateAsSQLDate() {
        return lastUpdateDate == null ? null : new java.sql.Date (lastUpdateDate.getTime());
    }

    public void setLastUpdateDate(@Changeable (LAST_UPDATE_DATE) Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(@Changeable (REGISTRATION_STATUS) String registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    public Date getNextRenewalDate() {
        return nextRenewalDate;
    }

    public java.sql.Date getNextRenewalDateAsSQLDate() {
        return nextRenewalDate == null ? null : new java.sql.Date (nextRenewalDate.getTime());
    }

    public void setNextRenewalDate(@Changeable (NEXT_RENEWAL_DATE) Date nextRenewalDate) {
        this.nextRenewalDate = nextRenewalDate;
    }

    public String getManagingLOU() {
        return managingLOU;
    }

    public void setManagingLOU(@Changeable (MANAGING_LOU) String managingLOU) {
        this.managingLOU = managingLOU;
    }

    public String getValidationSources() {
        return validationSources;
    }

    public void setValidationSources(@Changeable (VALIDATION_SOURCES) String validationSources) {
        this.validationSources = validationSources;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((initialRegistrationDate == null)
            ? 0 : initialRegistrationDate.hashCode());
        result = prime * result + ((lastUpdateDate == null) ? 0 : lastUpdateDate.hashCode());
        result = prime * result + ((managingLOU == null) ? 0 : managingLOU.hashCode());
        result = prime * result + ((nextRenewalDate == null) ? 0 : nextRenewalDate.hashCode());
        result = prime * result + ((registrationStatus == null) ? 0 : registrationStatus.hashCode());
        result = prime * result + ((validationSources == null) ? 0 : validationSources.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Registration other = (Registration) obj;
        if (initialRegistrationDate == null) {
            if (other.initialRegistrationDate != null)
                return false;
        } else if (!initialRegistrationDate.equals(other.initialRegistrationDate))
            return false;
        if (lastUpdateDate == null) {
            if (other.lastUpdateDate != null)
                return false;
        } else if (!lastUpdateDate.equals(other.lastUpdateDate))
            return false;
        if (managingLOU == null) {
            if (other.managingLOU != null)
                return false;
        } else if (!managingLOU.equals(other.managingLOU))
            return false;
        if (nextRenewalDate == null) {
            if (other.nextRenewalDate != null)
                return false;
        } else if (!nextRenewalDate.equals(other.nextRenewalDate))
            return false;
        if (registrationStatus == null) {
            if (other.registrationStatus != null)
                return false;
        } else if (!registrationStatus.equals(other.registrationStatus))
            return false;
        if (validationSources == null) {
            if (other.validationSources != null)
                return false;
        } else if (!validationSources.equals(other.validationSources))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Registration [initialRegistrationDate=" + initialRegistrationDate + ", lastUpdateDate="
            + lastUpdateDate + ", registrationStatus=" + registrationStatus + ", nextRenewalDate="
            + nextRenewalDate + ", managingLOU=" + managingLOU + ", validationSources="
            + validationSources + "]";
    }
}
