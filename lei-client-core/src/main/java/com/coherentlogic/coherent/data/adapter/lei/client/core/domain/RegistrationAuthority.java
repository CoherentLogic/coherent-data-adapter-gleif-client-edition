package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
"RegistrationAuthority": {
        "RegistrationAuthorityID": {
            "$": "RA000412"
        },
        "RegistrationAuthorityEntityID": {
            "$": "100001026624"
        }
    },
    "LegalJurisdiction": {
        "$": "JP"
    },
    "EntityStatus": {
        "$": "ACTIVE"
    }
},
 */
public class RegistrationAuthority extends SerializableBean {

    private static final long serialVersionUID = 1022229763422507247L;

    public static final String REGISTRATION_AUTHORITY_KEY = "RegistrationAuthority",
        REGISTRATION_AUTHORITY_ID_KEY = "RegistrationAuthorityID",
        REGISTRATION_AUTHORITY_ENTITY_ID_KEY = "RegistrationAuthorityEntityID";

    public static final String REGISTRATION_AUTHORITY_ID = "registrationAuthorityID",
        REGISTRATION_AUTHORITY_ENTITY_ID = "registrationAuthorityEntityID";

    private String registrationAuthorityID;

    private String registrationAuthorityEntityID;

    public String getRegistrationAuthorityID() {
        return registrationAuthorityID;
    }

    public void setRegistrationAuthorityID(
        @Changeable (REGISTRATION_AUTHORITY_ID) String registrationAuthorityID
    ) {
        this.registrationAuthorityID = registrationAuthorityID;
    }

    public String getRegistrationAuthorityEntityID() {
        return registrationAuthorityEntityID;
    }

    public void setRegistrationAuthorityEntityID(
        @Changeable (REGISTRATION_AUTHORITY_ENTITY_ID) String registrationAuthorityEntityID
    ) {
        this.registrationAuthorityEntityID = registrationAuthorityEntityID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
            + ((registrationAuthorityEntityID == null) ? 0 : registrationAuthorityEntityID.hashCode());
        result = prime * result + ((registrationAuthorityID == null)
            ? 0 : registrationAuthorityID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        RegistrationAuthority other = (RegistrationAuthority) obj;
        if (registrationAuthorityEntityID == null) {
            if (other.registrationAuthorityEntityID != null)
                return false;
        } else if (!registrationAuthorityEntityID.equals(other.registrationAuthorityEntityID))
            return false;
        if (registrationAuthorityID == null) {
            if (other.registrationAuthorityID != null)
                return false;
        } else if (!registrationAuthorityID.equals(other.registrationAuthorityID))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "RegistrationAuthority [registrationAuthorityID=" + registrationAuthorityID
            + ", registrationAuthorityEntityID=" + registrationAuthorityEntityID + "]";
    }
}
