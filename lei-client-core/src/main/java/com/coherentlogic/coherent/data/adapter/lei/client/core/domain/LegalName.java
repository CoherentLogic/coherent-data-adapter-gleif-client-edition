package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;

/**
"LegalName": {
    "@xml:lang": "ja",
    "$": "\u82b1\u5b9f\u5065\u98df\u682a\u5f0f\u4f1a\u793e"
}
 */
public class LegalName extends SerializableValueBean<String> implements XMLLangBeanSpecification {

    /**
     * 
     */
    private static final long serialVersionUID = 363186512056678190L;

    /**
     * @TODO Convert to an enum?
     */
    private String xmlLang;

    @Override
    public String getXMLLang() {
        return xmlLang;
    }

    @Override
    public void setXMLLang(@Changeable (XMLLANG) String xmlLang) {
        this.xmlLang = xmlLang;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((xmlLang == null) ? 0 : xmlLang.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        LegalName other = (LegalName) obj;
        if (xmlLang == null) {
            if (other.xmlLang != null)
                return false;
        } else if (!xmlLang.equals(other.xmlLang))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "LegalName [xmlLang=" + xmlLang + ", toString()=" + super.toString() + "]";
    }
}
