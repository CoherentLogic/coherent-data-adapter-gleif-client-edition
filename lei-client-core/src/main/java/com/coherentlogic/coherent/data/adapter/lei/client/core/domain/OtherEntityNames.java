package com.coherentlogic.coherent.data.adapter.lei.client.core.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/*
"OtherEntityNames": {
        "OtherEntityName": [{
            "@xml:lang": "en",
            "@type": "ALTERNATIVE_LANGUAGE_LEGAL_NAME",
            "$": "TUBOMIKENSHOKU CO.,LTD"
        }]
    },
 */
public class OtherEntityNames extends SerializableBean {

    private static final long serialVersionUID = 5180486507968857308L;

    private List<OtherEntityName> otherEntityNameList = new ArrayList<OtherEntityName> ();

    public List<OtherEntityName> getOtherEntityNameList() {
        return otherEntityNameList;
    }

    public static final String OTHER_ENTITY_NAME_LIST = "otherEntityNameList";

    public void setOtherEntityNameList(
        @Changeable (OTHER_ENTITY_NAME_LIST) List<OtherEntityName> otherEntityNameList
    ) {
        this.otherEntityNameList = otherEntityNameList;
    }

    public void addOtherEntityNames (OtherEntityName... otherEntityNames) {
        addOtherEntityNames (Arrays.asList(otherEntityNames));
    }

    public void addOtherEntityNames (List<OtherEntityName> otherEntityNames) {
        otherEntityNameList.addAll(otherEntityNames);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((otherEntityNameList == null) ? 0 : otherEntityNameList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        OtherEntityNames other = (OtherEntityNames) obj;
        if (otherEntityNameList == null) {
            if (other.otherEntityNameList != null)
                return false;
        } else if (!otherEntityNameList.equals(other.otherEntityNameList))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "OtherEntityNames [otherEntityNameList=" + otherEntityNameList + "]";
    }
}
