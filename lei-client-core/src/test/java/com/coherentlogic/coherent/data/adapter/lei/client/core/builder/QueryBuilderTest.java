package com.coherentlogic.coherent.data.adapter.lei.client.core.builder;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.coherentlogic.coherent.data.adapter.lei.client.core.builders.QueryBuilder;

public class QueryBuilderTest {

    private QueryBuilder queryBuilder = null;

    @BeforeEach
    public void setUp () {
        this.queryBuilder = new QueryBuilder (null);
    }

    @AfterEach
    public void tearDown () {
        this.queryBuilder = null;
    }

    @Test
    public void withLEI () {
        assertEquals ("https://leilookup.gleif.org/api/v2?lei=foo", queryBuilder.withLEI("foo").getEscapedURI());
    }
}
